{ lib, pkgs, qt, kde, isQt6, ... }: {
  colord-kde = {
    buildInputs = [ pkgs.lcms2 kde.kirigami-addons ]
      ++ (lib.optional (!isQt6) qt.qtx11extras);
  };
  gwenview = {
    buildInputs = with pkgs;
      [
        cfitsio
        exiv2
        lcms2
        qt.qtimageformats
        qt.qtsvg
        wayland
        wayland-protocols
      ] ++ (lib.optional (!isQt6) qt.qtx11extras)
      ++ (lib.optional (!isQt6) pkgs.libsForQt5.kimageannotator)
      ++ (lib.optional (!isQt6) pkgs.libsForQt5.kcolorpicker);
    # propagatedUserEnvPkgs = [ kde.kipi-plugins kde.libkipi ];
  };
  kdegraphics-mobipocket = { buildInputs = [ qt.qt5compat ]; };
  kdiagram = { buildInputs = [ qt.qtsvg ]; };
  libkdcraw = { propagatedBuildInputs = [ pkgs.libraw ]; };
  libkexiv2 = {
    buildInputs = [ qt.qt5compat ];
    propagatedBuildInputs = [ pkgs.exiv2 ];
  };
  okular = {
    buildInputs = with pkgs;
      [ discount djvulibre ebook_tools libspectre libzip chmlib kde.poppler ]
      ++ (with qt; [ qtdeclarative qtsvg qtspeech ])
      ++ (lib.optional (!isQt6) qt.qtx11extras);

    # InitialPreference values are too high and end up making okular
    # default for anything considered text/plain. Resetting to 1, which
    # is the default.
    postPatch = ''
      substituteInPlace generators/txt/okularApplication_txt.desktop \
        --replace InitialPreference=3 InitialPreference=1
    '';
  };
  spectacle = {
    buildInputs = [ pkgs.wayland pkgs.xorg.libXdmcp pkgs.xcb-util-cursor ]
      ++ lib.optional (!isQt6) qt.qtquickcontrols2
      ++ (lib.optional (!isQt6) qt.qtx11extras)
      ++ lib.optional (!isQt6) pkgs.xorg.libXdmcp;
  };
}
