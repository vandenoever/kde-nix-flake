{ lib, pkgs, qt, isQt6, kde, ... }: {
  dolphin = {
    buildInputs = lib.optional (!isQt6) qt.qtx11extras;
    # passthru.tests.test = nixosTests.terminal-emulators.konsole;
  };
  khelpcenter = {
    buildInputs = [ pkgs.xapian kde.grantlee ]
      ++ lib.optional isQt6 qt.qtwebengine;
  };
  kio-admin = { buildInputs = [ pkgs.git ]; };
}
