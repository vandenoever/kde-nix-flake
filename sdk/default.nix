{ lib, pkgs, qt, ... }: {
  dolphin-plugins = { buildInputs = [ qt.qt5compat ]; };
  selenium-webdriver-at-spi = { buildInputs = [ pkgs.wayland ]; };
}
