{ lib, pkgs, qt, data, isQt6 }:
(lib.makeScope pkgs.newScope (self:
  let
    kde = self;
    propagate = out:
      let
        setupHook = { writeScript }:
          writeScript "setup-hook" ''
            if [ "''${hookName:-}" != postHook ]; then
                postHooks+=("source @dev@/nix-support/setup-hook")
            else
                # Propagate $dev so that this setup hook is propagated
                # But only if there is a separate $dev output
                if [ "''${outputDev:?}" != out ]; then
                    propagatedBuildInputs="''${propagatedBuildInputs-} @dev@"
                fi
            fi
          '';
      in kde.callPackage setupHook { };
    attrs = { inherit lib pkgs qt kde propagate isQt6; };
    specific = (import ./education attrs) // (import ./frameworks attrs)
      // (import ./graphics attrs) // (import ./libraries attrs)
      // (import ./multimedia attrs) // (import ./network attrs)
      // (import ./pim attrs) // (import ./plasma attrs) // (import ./sdk attrs)
      // (import ./system attrs) // (import ./utilities attrs);
    mkDerivation = kde.callPackage
      ({ stdenv, mkDerivation ? stdenv.mkDerivation }: mkDerivation) { };
    gpgme = pkgs.gpgme.override { qtbase = qt.qtbase; };
    qtkeychain = import ./libraries/qtkeychain.nix ({
      inherit lib;
      inherit (pkgs.darwin.apple_sdk.frameworks) CoreFoundation Security;
      inherit (pkgs) stdenv pkg-config fetchFromGitHub cmake libsecret;
      inherit (qt) qttools qtbase;
    });
    qcoro = import ./libraries/qcoro.nix ({
      inherit lib;
      inherit (pkgs.xorg) libpthreadstubs;
      inherit (pkgs) stdenv fetchFromGitHub cmake;
      inherit (qt) qtbase wrapQtAppsHook qtwebsockets;
    });
    poppler =
      if isQt6 then pkgs.qt6Packages.poppler else pkgs.libsForQt5.poppler;
    appstream-qt = if isQt6 then
      pkgs.libsForQt5.appstream-qt
    else
      pkgs.libsForQt5.appstream-qt;
    grantlee = pkgs.libsForQt5.grantlee;
    kdsoap = import ./libraries/kdsoap.nix {
      inherit pkgs mkDerivation isQt6;
      qtbase = qt.qtbase;
    };
    libquotient = import ./libraries/libquotient.nix {
      inherit (pkgs) openssl cmake stdenv fetchFromGitHub olm;
      inherit lib qtkeychain isQt6;
      qtbase = qt.qtbase;
      qtmultimedia = qt.qtmultimedia;
    };
    additionalCMakeFlags = lib.optional isQt6 "-DBUILD_WITH_QT6=ON";
    packages = builtins.mapAttrs (name: pkg:
      let args = { dontWrapQtApps = false; } // (specific.${name} or { });
      in mkDerivation (args // {
        pname = args.pname or name;
        version = args.version or pkg.version;
        src = args.src or (pkgs.fetchgit {
          url = "https://invent.kde.org/${pkg.repopath}.git";
          rev = pkg.rev;
          sha256 = pkg.sha256;
        });
        outputs = args.outputs or [ "out" "dev" ];
        # opt-in to qt.wrapQtAppsHook for all packages
        buildInputs = (args.buildInputs or [ ]) ++ (if args.dontWrapQtApps then
          [ ]
        else [
          qt.wrapQtAppsHook
          # wayland is not part of wrapQtAppsHook but it is needed for gui apps
          qt.qtwayland
        ]);
        propagatedBuildInputs = (args.propagatedBuildInputs or [ ])
          ++ [ qt.qtbase qt.qttools ]
          ++ (map (name: self.${name}) pkg.dependencies);
        cmakeFlags = (args.cmakeFlags or [ ]) ++ additionalCMakeFlags;
      })) data;
  in packages // {
    inherit gpgme qtkeychain poppler grantlee appstream-qt kdsoap qcoro
      libquotient;
  }))
