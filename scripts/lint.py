#!/usr/bin/env python
"""
This script runs a number of linting tools in parallel.

By default it runs on files that are different than the files in the git HEAD.
"""
import argparse
import sys
from concurrent.futures import ThreadPoolExecutor, as_completed
from dataclasses import dataclass, field
from fnmatch import fnmatch
from pathlib import Path
from subprocess import run
from typing import List

from git.objects import Blob
from git.repo import Repo


@dataclass
class Test:
    ext: str
    exe: str
    check_args: List[str] = field(default_factory=list)
    fix_args: List[str] = field(default_factory=list)
    exclude_globs: List[str] = field(default_factory=list)


@dataclass
class Result:
    cmd: str
    files_checked: int = 0
    stdout: str = ""
    stderr: str = ""
    exit_code: int = 0


class colors:
    OK = "\033[92m"
    ERROR = "\033[93m"
    NONE = "\033[0m"


def get_files(check_all: bool) -> List[Path]:
    """Get all files that are in HEAD and any staged files."""
    repo = Repo(".")
    repo_files: List[str] = []
    if check_all:
        for entry in repo.commit().tree.traverse():
            if isinstance(entry, Blob) and isinstance(entry.path, str):
                repo_files.append(entry.path)
    changed_files = [item.a_path for item in repo.index.diff(None)]
    added_files = [entry.a_path for entry in repo.index.diff("HEAD")]

    files = set(repo_files + changed_files + added_files)
    paths = [Path(path) for path in files]
    return [path for path in paths if path.exists() and path.is_file()]


def match_glob(value: str, globs: List[str]) -> bool:
    """Return true if the value matches any of the glob patterns."""
    for glob in globs:
        if fnmatch(value, glob):
            return True
    return False


def run_test(files: List[Path], test: Test, ci_mode: bool) -> Result:
    suffix = "." + test.ext
    args = [str(file) for file in files if file.suffix == suffix]
    args = [file for file in args if not match_glob(file, test.exclude_globs)]
    if not ci_mode and test.fix_args:
        cmd = [test.exe] + test.fix_args + args
    else:
        cmd = [test.exe] + test.check_args + args
    if len(args) == 0:
        return Result(test.exe)
    result = run(cmd, check=False, capture_output=True, text=True)
    return Result(test.exe, len(args), result.stdout, result.stderr, result.returncode)


def main() -> None:
    parser = argparse.ArgumentParser(
        description="Run checks and fixes on files in the repository",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "--ci-mode", help="Check all files, do not fix", action="store_true"
    )
    parser.add_argument(
        "--all", help="Check all files, not just the changed files", action="store_true"
    )
    args = parser.parse_args()
    files = get_files(args.ci_mode or args.all)
    tests = [
        Test("py", "pylint"),
        Test("py", "mypy", ["--strict", "--pretty"]),
        Test(
            "py",
            "isort",
            ["--profile", "black", "--check"],
            ["--profile", "black", "--overwrite-in-place"],
        ),
        Test("py", "black", ["--check"], ["-q"]),
        Test("nix", "nixfmt", ["--check"], ["--verify"]),
        Test(
            "sh",
            "shellcheck",
            [],
            [],
            [
                "frameworks/extra-cmake-modules/setup-hook.sh",
                "frameworks/kdelibs4support/setup-hook.sh",
            ],
        ),
    ]
    error = False
    with ThreadPoolExecutor(len(tests)) as executor:
        futures = [
            executor.submit(lambda t: run_test(files, t, args.ci_mode), test)
            for test in tests
        ]
        for future in as_completed(futures):
            result = future.result()
            if result.exit_code == 0:
                print(
                    f"{colors.OK}{result.cmd}: Ok, "
                    + f"{result.files_checked} files checked.{colors.NONE}"
                )
            else:
                print(f"{colors.ERROR}{result.cmd}: Error{colors.NONE}")
                print(result.stdout)
                print(result.stderr)
                error = True

    if error:
        sys.exit(1)


if __name__ == "__main__":
    main()
