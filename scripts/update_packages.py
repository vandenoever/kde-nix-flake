#! /usr/bin/env python3
"""
This script reads information from KDE repositories and writes it to
`packages.json` and `versions.json`.

If the KDE repositories are not available, it will retrieve them from the
KDE git server (https://invent.kde.org).
"""
import argparse
from pathlib import Path

from modules.data_models import write
from modules.load import load_packages


def main() -> None:
    parser = argparse.ArgumentParser(
        description="Update the package description files",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "--invent-dir", help="Path to repositories from invent.kde.org", required=True
    )
    parser.add_argument(
        "--fetch", help="Fetch updates of the repositories", action="store_true"
    )
    parser.add_argument(
        "--packages_output", help="File to write to", default="packages.json"
    )
    parser.add_argument(
        "--versions_output", help="File to write to", default="versions.json"
    )
    args = parser.parse_args()
    invent_path = Path(args.invent_dir)
    packages, versions = load_packages(invent_path, args.fetch)
    write(args.packages_output, packages)
    write(args.versions_output, versions)


if __name__ == "__main__":
    main()
