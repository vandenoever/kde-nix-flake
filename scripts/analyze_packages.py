#! /usr/bin/env python3
"""
This script analyzes KDE repositories.

If the KDE repositories are not available, it will retrieve them from the
KDE git server (https://invent.kde.org).
"""
import argparse
from typing import Dict, List, Set

from modules.data_models import Package, Packages, read


def remove_unlinked(packages: Dict[str, Package]) -> None:
    """Remove all packages that have no dependencies and are not named as dependencies"""
    used = set()
    keys: List[str] = list(packages.keys())
    for key in keys:
        package = packages[key]
        for d in package.dependencies:
            used.add(d)
    for key in keys:
        if key not in used and not packages[key].dependencies:
            packages.pop(key)


def sort_by_graph(packages: Dict[str, Package]) -> Dict[str, int]:
    sort: Dict[str, int] = {}
    keys: Set[str] = set(sorted(packages.keys()))
    depth = 0
    while keys:
        removed = []
        for key in keys:
            package = packages[key]
            all_found = True
            for d in package.dependencies:
                if d not in sort:
                    all_found = False
                    break
            if all_found:
                removed.append(key)
        for r in removed:
            sort[r] = depth
            keys.remove(r)
        depth += 1

    return sort


def main() -> None:
    parser = argparse.ArgumentParser(
        description="Analyze the files in the KDE repository",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    _args = parser.parse_args()
    # packages, _ = load_packages(invent_path, False)
    packages = read("packages.json", Packages)
    remove_unlinked(packages.__root__["kf5-qt5"])
    sort = sort_by_graph(packages.__root__["kf5-qt5"])
    print("DONE")
    print(sort)


if __name__ == "__main__":
    main()
