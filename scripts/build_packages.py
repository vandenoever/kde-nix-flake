#!/usr/bin/env python

""" This script builds the flake
It reports on what worked and what did not work.
The build is analyzed by using the bar-with-logs log format of `nix build`.
"""
import argparse
import json
import subprocess
import sys
from typing import Dict, List, Tuple

from modules.data_models import Version, Versions, read, write
from modules.nix_log_parser import ParseError, Parser
from pydantic import BaseModel


def get_package_names(layer: str) -> Dict[str, Tuple[str, str]]:
    """Return a dict with name-version and name-rev[:7] as keys"""
    with open("versions.json", "r", encoding="utf8") as file:
        versions = json.load(file)[layer]
    with open("packages.json", "r", encoding="utf8") as file:
        packages = json.load(file)[layer]
    package_names = {}
    for name, value in versions.items():
        version = value["version"]
        # the last element of repopath is used in prefixe log lines 'name> '
        short_name = packages[name]["repopath"].split("/")[-1]
        rev = value["rev"][:7]
        package_names[name + "-" + version] = (name, version)
        package_names[short_name + "-" + rev] = (short_name, version)
    # The package for building everything
    package_names["all"] = ("", "")
    return package_names


def build_layer(layer: str) -> List[str]:
    parser = Parser()
    with subprocess.Popen(
        ["nix", "build", "--log-format", "bar-with-logs", "--keep-going", f".#{layer}"],
        stdout=subprocess.DEVNULL,
        stderr=subprocess.PIPE,
    ) as proc:
        if not proc.stderr:
            raise ValueError("No stderr available")
        while proc.poll() is None:
            # Output of `nix build` is mostly utf8 but not always, so do lossy
            # parsing of the bytes to UTF-8.
            byteline = proc.stderr.readline()
            if byteline.endswith(b"\n"):
                byteline = byteline[:-1]
            line = byteline.decode("utf-8", errors="replace")
            print(line, flush=True, file=sys.stderr)
            if line:
                try:
                    parser.parse(line)
                except ParseError as e:
                    # save the nix log before quiting, so the parse error can
                    # be debugged with `test_build_log.py`
                    log_file = f"build_{layer}_output.log"
                    print(f"Error parsing: {e}", file=sys.stderr)
                    parser.save_log(log_file)
                    print(f"Wrote log to {log_file}", file=sys.stderr)
                    raise e

    log = parser.finish()
    print(f"Tried building {len(log.package_logs)} packages in layer {layer}.")
    write(f"out_{layer}.json", log)
    failed = []
    for name, package in log.package_logs.items():
        if not package.ok:
            failed.append(name[33:])
    return failed


def get_version_names(layer_version: Dict[str, Version]) -> Dict[str, str]:
    version_names = {}
    for name, version in layer_version.items():
        v = version.version.replace("^", "-").replace("~", "-")
        version_names[f"{name}-{v}"] = name
    return version_names


def get_known_failed(version_names: Dict[str, str], failed: List[str]) -> List[str]:
    known_failed = []
    for f in failed:
        if f in version_names:
            known_failed.append(version_names[f])
    return sorted(known_failed)


class Failing(BaseModel):
    __root__: Dict[str, List[str]]


def add_failing(current: List[str], new: List[str]) -> List[str]:
    for n in new:
        if n not in current:
            current.append(n)
    return sorted(current)


def main() -> None:
    parser = argparse.ArgumentParser(
        description="Build the packages and update `failed.json`.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "--try-all",
        help="Try to build all packages, not just the passing ones",
        action="store_true",
    )
    args = parser.parse_args()
    versions = read("versions.json", Versions)
    failing = read("failing.json", Failing)
    # build all packages and record which are failing
    for layer, layer_version in versions.__root__.items():
        # write an empty array in failing.json for this layer, so that
        # `nix build` tries to build everything in that layer
        if args.try_all:
            failing.__root__[layer] = []
            write("failing.json", failing)
        version_names = get_version_names(layer_version)
        failed = build_layer(layer)
        known_failed = get_known_failed(version_names, failed)
        failing.__root__[layer] = add_failing(failing.__root__[layer], known_failed)
        write("failing.json", failing)


if __name__ == "__main__":
    main()
