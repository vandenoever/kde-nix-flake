from datetime import datetime
from typing import Dict, List, Optional, Type, TypeVar

from pydantic import BaseModel, parse_file_as


class Package(BaseModel):
    name: str
    projectpath: str
    repopath: str
    branch: str
    dependencies: List[str] = []
    tier: Optional[int]
    type: Optional[str]


class Packages(BaseModel):
    __root__: Dict[str, Dict[str, Package]]


class Version(BaseModel):
    version: str
    date: datetime
    rev: str
    sha256: str


class Versions(BaseModel):
    __root__: Dict[str, Dict[str, Version]]


T = TypeVar("T")


def read(input_path: str, cls: Type[T]) -> T:
    return parse_file_as(path=input_path, type_=cls)


def write(output_path: str, data: BaseModel) -> None:
    print(f"Saving to {output_path}")
    data_json = data.json(exclude_none=True, indent=4)
    with open(output_path, "w", encoding="utf-8") as out_file:
        out_file.write(data_json)
        out_file.close()
