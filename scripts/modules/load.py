#! /usr/bin/env python3
import subprocess
import sys
from concurrent.futures import ThreadPoolExecutor
from dataclasses import dataclass
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Any, Dict, List, Optional, Set, Tuple, Type, TypeVar

import yaml
from git import GitCommandError
from git.objects import Commit
from git.repo import Repo
from modules.data_models import Package, Packages, Version, Versions
from modules.info import BranchInfo, Dependency, Info, KdeCi, Metadata, Metainfo
from modules.lms import LogicalModuleStructure, get_logical_module_structure
from modules.sha256_cache import Sha256Cache
from modules.version import get_version
from pydantic import ValidationError

ignored_components = [
    "websites",
    "wikitolearn",
    "webapps",
    "historical",
    "documentation",
    "sysadmin",
    "neon",
    "packaging",
]


def _get_repo_metadata(path: Path) -> List[Metadata]:
    """Read the metadata.yaml files in the repo-metadata repository"""
    metadatas = []
    projects_invent = Path(path, "projects")
    for filepath in projects_invent.glob("*/*/metadata.yaml"):
        with open(filepath, "r", encoding="utf-8") as file:
            metadata_yaml = yaml.safe_load(file)
            metadata = Metadata(**metadata_yaml)
        repopath = filepath.relative_to(projects_invent).parent
        if repopath.parent.name in ignored_components:
            continue
        if metadata.repopath != str(repopath):
            raise ValueError(f"{repopath} != {metadata.repopath} in {filepath}")
        metadatas.append(metadata)
    return metadatas


def _create_repo(path: Path, repopath: str) -> Repo:
    path = Path(path, repopath)
    if path.exists():
        repo = Repo(path)
    else:
        repo = Repo.clone_from("git@invent.kde.org:" + repopath, path)
    return repo


def _read_yaml_from_commit(commit: Commit, name: str) -> Optional[Any]:
    for blob in commit.tree.blobs:
        if blob.name == name:
            return yaml.safe_load(blob.data_stream)
    return None


def _get_sha256(sha1: str, repodir: str) -> str:
    with TemporaryDirectory() as tmp_dir:
        with subprocess.Popen(
            ("git", "archive", sha1),
            stdout=subprocess.PIPE,
            cwd=repodir,
        ) as git_process:
            subprocess.run(
                ("tar", "x", "-C", tmp_dir), check=True, stdin=git_process.stdout
            )
        result = subprocess.run(
            ("nix", "hash", "path", "--base32", tmp_dir),
            check=True,
            text=True,
            stdout=subprocess.PIPE,
        )
    return result.stdout.strip()


T = TypeVar("T")


def _parse_yaml(
    commit: Commit, name: str, repopath: str, branch_name: str, cls: Type[T]
) -> Optional[T]:
    blob = _read_yaml_from_commit(commit, name)
    if blob:
        try:
            instance = cls(**blob)
        except ValidationError as e:
            raise ValueError(
                f"Could not parse {name} for {branch_name} in {repopath}."
            ) from e
    else:
        # disable print because too frequent and noisy
        # print(
        #     f"Branch {branch_name} in {repopath} has no {name}",
        #     file=sys.stderr,
        # )
        instance = None
    return instance


def _get_branch_info(
    sha256_cache: Sha256Cache,
    repo: Repo,
    repopath: str,
    branch_name: str,
) -> Optional[BranchInfo]:
    full_branch_name = "origin/" + branch_name
    commit = None
    for b in repo.remote().refs:
        if b.name == full_branch_name:
            commit = b.commit
            break
    if not commit:
        # disable print because too frequent and noisy
        # print(f"No branch {branch_name} in {repopath}", file=sys.stderr)
        return None
    rev = commit.hexsha
    date_time = commit.committed_datetime
    sha256 = sha256_cache.get(rev)
    if not sha256:
        sha256 = _get_sha256(rev, str(repo.working_dir))
        sha256_cache.add(rev, sha256)
    kde_ci = _parse_yaml(commit, ".kde-ci.yml", repopath, branch_name, KdeCi)
    metainfo = _parse_yaml(commit, "metainfo.yaml", repopath, branch_name, Metainfo)
    version = get_version(commit)
    return BranchInfo(branch_name, rev, date_time, sha256, metainfo, kde_ci, version)


def _get_branches_info(
    sha256_cache: Sha256Cache, repo: Repo, repopath: str, branches: Set[str]
) -> Dict[str, BranchInfo]:
    branches_info = {}
    for branch in branches:
        try:
            branch_info = _get_branch_info(sha256_cache, repo, repopath, branch)
            if branch_info:
                branches_info[branch] = branch_info
        except GitCommandError as _e:
            pass
    return branches_info


def _get_info(branch: BranchInfo, layer: str) -> Info:
    if branch.kde_ci:
        dependencies = _list_dependencies(branch.kde_ci, layer)
    else:
        dependencies = []
    return Info(branch, dependencies)


def _update_repository(repo: Repo, branch: Optional[str]) -> None:
    if branch:
        remote = repo.remote()
        try:
            remote.fetch(branch)
        except GitCommandError as e:
            if "The requested URL returned error: 429" in str(e):
                raise RuntimeError(
                    "Server complains: too many requests. Aborting."
                ) from e
            url = next(remote.urls)
            print(
                f"There is no branch {branch} in {url}: {e}",
                file=sys.stderr,
            )


@dataclass
class ProcessResult:
    project: Metadata
    info: Dict[str, Info]


def _get_exceptional_branch(layer: str, identity: str) -> Optional[str]:
    if layer == "kf6-qt6":
        match identity:
            case "kdsoap-ws-discovery-client":
                return "work/nico/qt6"
    return None


def _get_layer_branches(
    project: Metadata, lms: LogicalModuleStructure
) -> Dict[str, str]:
    branches = {}
    for layer in lms.layers:
        branch = _get_exceptional_branch(layer, project.identifier)
        if not branch:
            branch = lms.get_branch(layer, project.projectpath)
        if branch:
            branches[layer] = branch
    return branches


def _update_branches(repo: Repo, branches: Set[str]) -> None:
    for branch in list(branches):
        try:
            _update_repository(repo, branch)
        except GitCommandError as _e:
            # branch is not available
            branches.remove(branch)


def _process_project(
    project: Metadata,
    lms: LogicalModuleStructure,
    sha256_cache: Sha256Cache,
    fetch: bool,
    invent_path: Path,
) -> ProcessResult:
    info: Dict[str, Info] = {}
    if not project.repoactive:
        return ProcessResult(project, info)
    with _create_repo(invent_path, project.repopath) as repo:
        layer_branches = _get_layer_branches(project, lms)
        branches = set(layer_branches.values())
        if fetch:
            _update_branches(repo, branches)
        branches_info = _get_branches_info(
            sha256_cache, repo, project.repopath, branches
        )
        for layer in lms.layers:
            branch = layer_branches.get(layer)
            branch_info = branch and branches_info.get(branch)
            if branch_info:
                info[layer] = _get_info(branch_info, layer)
    return ProcessResult(project, info)


def _find_project_identifier(
    projects: Dict[str, Package], projectpath: str
) -> Optional[str]:
    for identifier, project in projects.items():
        if project.projectpath == projectpath:
            return identifier
    for identifier, project in projects.items():
        if project.repopath == projectpath:
            return identifier
    return None


def _remove_indirect_dependencies(projects: Dict[str, Package]) -> None:
    for project in projects.values():
        # loop over a copy of the dependencies
        deps = set(project.dependencies)
        for dep_id in project.dependencies:
            for d in projects[dep_id].dependencies:
                if d in deps:
                    deps.remove(d)
        project.dependencies = sorted(deps)


def _add_dependencies(
    deps: Dict[str, List[str]], projects: Dict[str, Package]
) -> Set[str]:
    """Add the missing dependencies to projects.
    Return a set of dependencies that could not be found."""
    missing = set()
    for identifier, project in projects.items():
        for dep in deps[identifier]:
            dep_id = _find_project_identifier(projects, dep)
            if dep_id:
                project.dependencies.append(dep_id)
            else:
                missing.add(dep)
    _remove_indirect_dependencies(projects)
    return missing


def _get_dependencies(deps: List[Dependency], layer: str) -> List[str]:
    result = []
    for dep in deps:
        should_include = False
        for v in dep.on:
            match v:
                case "@all" | "Linux":
                    should_include = True
                case "Linux/Qt6":
                    if layer == "kf6-qt6":
                        should_include = True
                case "Linux/Qt5":
                    if layer in ("kf5-qt5", "stable-kf5-qt5"):
                        should_include = True
                case "FreeBSD" | "FreeBSD/Qt6" | "FreeBSD/Qt5":
                    pass
                case "Windows" | "Windows/Qt5" | "Windows/Qt6":
                    pass
                case "Android" | "Android/Qt5" | "Android/Qt6":
                    pass
                case "macOS" | "macOS/Qt5" | "macOS/Qt6" | "iOS":
                    pass
                case _:
                    raise ValueError(f"Unknown platform: {v}")
        if should_include:
            for r in dep.require.keys():
                result.append(r)
    return result


def _list_dependencies(kdeci: KdeCi, layer: str) -> List[str]:
    dependencies = _get_dependencies(kdeci.Dependencies, layer)
    runtime_dependencies = _get_dependencies(kdeci.RuntimeDependencies, layer)
    return dependencies + runtime_dependencies


def _info_to_package(
    results: List[ProcessResult],
) -> Tuple[Packages, Versions]:
    package_dict: Dict[str, Dict[str, Package]] = {}
    version_dict: Dict[str, Dict[str, Version]] = {}
    deps: Dict[str, Dict[str, List[str]]] = {}
    for result in results:
        project = result.project
        for layer, info in result.info.items():
            group_dict = package_dict.setdefault(layer, {})
            group_dict[project.identifier] = Package(
                name=project.name,
                projectpath=project.projectpath,
                repopath=project.repopath,
                branch=info.branch.branch,
                dependencies=[],
                tier=info.branch.metainfo and info.branch.metainfo.tier,
                type=info.branch.metainfo and info.branch.metainfo.type,
            )
            v_dict = version_dict.setdefault(layer, {})
            v_dict[project.identifier] = Version(
                version=info.branch.version,
                rev=info.branch.rev,
                date=info.branch.date,
                sha256=info.branch.sha256,
            )
            deps.setdefault(layer, {})[project.identifier] = info.dependencies

    for layer, packages in package_dict.items():
        packages = dict(sorted(packages.items()))
        package_dict[layer] = packages
        missing = _add_dependencies(deps[layer], packages)
        print(f"Missing dependencies in {layer}: {missing}")
    package_dict = dict(sorted(package_dict.items()))
    for layer, versions in version_dict.items():
        versions = dict(sorted(versions.items()))
        version_dict[layer] = versions
    version_dict = dict(sorted(version_dict.items()))
    return (Packages(__root__=package_dict), Versions(__root__=version_dict))


def load_packages(invent_path: Path, fetch: bool) -> Tuple[Packages, Versions]:
    metadata_path = Path(invent_path, "sysadmin/repo-metadata")
    metadata_repo = _create_repo(invent_path, "sysadmin/repo-metadata")
    if fetch:
        _update_repository(metadata_repo, "master")
    lms = get_logical_module_structure(metadata_path, default_branch="master")
    metadata = _get_repo_metadata(metadata_path)
    all_results: List[ProcessResult] = []
    sha256_cache = Sha256Cache()

    # when doing fetch, do not overload the git server with too many parallel
    # requests
    threads = 4 if fetch else None
    with ThreadPoolExecutor(threads) as executor:
        results = executor.map(
            lambda p: _process_project(p, lms, sha256_cache, fetch, invent_path),
            metadata,
        )
        all_results = list(results)
    return _info_to_package(all_results)
