import re
import subprocess
from datetime import datetime
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Optional

from git.objects import Commit


def read_text_from_commit(commit: Commit, name: str) -> Optional[str]:
    for blob in commit.tree.blobs:
        if blob.name == name:
            text: str = blob.data_stream.read().decode("utf-8")
            return text
    return None


project_version_matcher = re.compile(
    r"\bproject\s*\(\s*[\w-]+\s+version\s+[\"']?([\w.-]+)[\"'\s\)]", re.IGNORECASE
)
variable_version_matcher = re.compile(
    r"\bproject\s*\(\s*[\w-]+\s+version\s+\$\{(\w+)\}[\s\)]", re.IGNORECASE
)
project2_version_matcher = re.compile(
    r"\bset\s*\(\s*project_version\s+[\"']?([\w.-]+)[\"']?\s*\)", re.IGNORECASE
)
set_version_matcher = re.compile(
    r"\bset\s*\(\s*(\w+)\s+[\"']?([\w.-]+)[\"']?\s*\)", re.IGNORECASE
)


def find_variable_version(cmakelists: str, variable: str) -> Optional[str]:
    for m in set_version_matcher.findall(cmakelists):
        if m[0] == variable:
            return str(m[1])
    return None


def find_version(commit: Commit) -> Optional[str]:
    cmakelists: Optional[str] = read_text_from_commit(commit, "CMakeLists.txt")
    if cmakelists:
        if m := project_version_matcher.search(cmakelists):
            return m[1]
        if m := project2_version_matcher.search(cmakelists):
            return m[1]
        if m := variable_version_matcher.search(cmakelists):
            if version := find_variable_version(cmakelists, m[1]):
                return version
        return find_version_with_cmake(cmakelists)
    return None


def get_version(commit: Commit) -> str:
    """Return a snapshot version number according to
    https://docs.fedoraproject.org/en-US/packaging-guidelines/Versioning/
    """
    version = find_version(commit)
    if version is None:
        version = "0"  # use an obviously wrong version number
    date = datetime.fromtimestamp(commit.committed_date).strftime("%Y%m%d")
    return version + "^" + date + "git" + str(commit.hexsha[:7])


def find_version_with_cmake(cmakelists: str) -> Optional[str]:
    """Run cmake on a directory with only a CMakeLists.txt file.
    This will fail, but also write a CMakeCache.txt file that may contain
    the project version.
    In many projects, even this fails, because the version is set after
    detecting the dependencies."""
    with TemporaryDirectory() as tmp_dir:
        out_path = Path(tmp_dir, "CMakeLists.txt")
        with open(out_path, "w", encoding="utf-8") as file:
            file.write(cmakelists)
            file.close()
        subprocess.run(
            ("cmake", "-S", tmp_dir, "-B", tmp_dir),
            check=False,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
        )
        cache_path = Path(tmp_dir, "CMakeCache.txt")
        prefix = "CMAKE_PROJECT_VERSION:STATIC="
        with open(cache_path, "r", encoding="utf-8") as file:
            for line in file.readlines():
                line = line.rstrip()
                if line.startswith(prefix):
                    version = line[len(prefix) :]
                    return version
    return None
