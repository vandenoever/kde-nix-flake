import json
from pathlib import Path
from typing import Dict


class Sha256Cache:
    cache: Dict[str, str]
    initial_size: int

    def __init__(self) -> None:
        try:
            cache_json = Path("sha256_cache.json").read_text(encoding="utf-8")
            self.cache = json.loads(cache_json)
        except FileNotFoundError:
            self.cache = {}
        self.initial_size = len(self.cache)

    def __del__(self) -> None:
        # save the cache to file if entries were added
        if len(self.cache) != self.initial_size:
            json_str = json.dumps(self.cache, sort_keys=True, indent=4)
            Path("sha256_cache.json").write_text(json_str, encoding="utf-8")

    def get(self, sha1: str) -> str | None:
        return self.cache.get(sha1)

    def add(self, sha1: str, sha256: str) -> None:
        self.cache[sha1] = sha256
