{ lib, pkgs, qt, kde, isQt6, propagate, ... }: {
  bluedevil = { buildInputs = [ pkgs.shared-mime-info ]; };
  breeze = {
    propagatedUserEnvPkgs = lib.optional (!isQt6) qt.qtgraphicaleffects;
    propagatedBuildInputs = [ qt.qtdeclarative pkgs.fftw ];
    cmakeFlags = [ "-DBUILD_QT5=OFF" ];
    outputs = [ "bin" "dev" "out" ];
  };
  # breeze-grub produces no output
  breeze-grub = { };
  breeze-gtk = {
    nativeBuildInputs = with pkgs; [ sassc python3 python3.pkgs.pycairo ];
    patches = [ ./breeze-gtk/0001-fix-add-executable-bit.patch ];
    postPatch = ''
      sed -i cmake/FindGTKEngine.cmake \
        -e "s|\''${KDE_INSTALL_FULL_LIBDIR}|${lib.getLib pkgs.gtk2}/lib|"
    '';
    cmakeFlags = [ "-DWITH_GTK3_VERSION=3.22" ];
  };
  breeze-plymouth = {
    buildInputs = [ pkgs.plymouth ];
    patches = [ ./breeze-plymouth/install-paths.patch ];
  };
  discover = { buildInputs = [ qt.qtwebview kde.appstream-qt ]; };
  drkonqi = { buildInputs = with kde; [ kdeclarative kirigami kitemmodels ]; };
  kactivitymanagerd = { buildInputs = [ qt.qt5compat pkgs.boost ]; };
  kde-cli-tools = {
    buildInputs = [ qt.qtsvg qt.wrapQtAppsHook ];
    dontWrapQtApps = true;
    preFixup = ''
      for program in $out/bin/*; do
        wrapQtApp $program
      done
    '';
  };
  kde-gtk-config = {
    nativeBuildInputs = [ pkgs.wrapGAppsHook ];
    dontWrapGApps = true; # There is nothing to wrap
    buildInputs = with pkgs; [
      qt.qtsvg
      glib
      gtk2
      gtk3
      gsettings-desktop-schemas
      xsettingsd
      sass
    ];
    cmakeFlags = [
      "-DGTK2_GLIBCONFIG_INCLUDE_DIR=${pkgs.glib.out}/lib/glib-2.0/include"
      "-DGTK2_GDKCONFIG_INCLUDE_DIR=${pkgs.gtk2.out}/lib/gtk-2.0/include"
      "-DGLIB_SCHEMAS_DIR=${pkgs.gsettings-desktop-schemas.out}/"
    ];
    # The gtkconfig KDED module will crash the daemon if the GSettings schemas
    # aren't found.
    patches = [ ./kde-gtk-config/0001-gsettings-schemas-path.patch ];
    preConfigure = ''
      NIX_CFLAGS_COMPILE+=" -DGSETTINGS_SCHEMAS_PATH=\"$GSETTINGS_SCHEMAS_PATH\""
    '';
  };
  kdeplasma-addons = { buildInputs = [ qt.qt5compat ]; };
  khotkeys = {
    outputs = [ "out" ];
    buildInputs = lib.optional (!isQt6) qt.qtx11extras;
  };
  kpipewire = {
    buildInputs = with pkgs; [ ffmpeg mesa pipewire wayland libva ];
    propagatedBuildInputs = [ pkgs.libepoxy ];
  };
  kgamma5 = { buildInputs = [ pkgs.xorg.libXxf86vm ]; };
  kscreen = {
    buildInputs = [ qt.qtsensors ] ++ (lib.optional (!isQt6) qt.qtx11extras);
  };
  kscreenlocker = {
    buildInputs = with pkgs;
      [ xorg.libXcursor pam qt.qtdeclarative wayland ]
      ++ (lib.optional (!isQt6) qt.qtx11extras);
  };
  ksystemstats = {
    env.NIX_CFLAGS_COMPILE =
      toString [ "-I${lib.getBin kde.libksysguard}/share" ];
    buildInputs = with pkgs; [
      libnl
      lm_sensors
      # networkmanager-qt
    ];
  };
  kwallet-pam = {
    buildInputs = with pkgs; [ pam socat libgcrypt qt.wrapQtAppsHook ];
    postPatch = ''
      sed -i pam_kwallet_init -e "s|socat|${lib.getBin pkgs.socat}/bin/socat|"
    '';

    # We get a crash when QT_PLUGIN_PATH is more than 1000 characters.
    # pam_kwallet_init passes its environment to kwalletd5, but
    # wrapQtApps gives our environment a huge QT_PLUGIN_PATH value. We
    # are able to unset it here since kwalletd5 will have its own
    # QT_PLUGIN_PATH.
    postFixup = ''
      wrapProgram $out/libexec/pam_kwallet_init --unset QT_PLUGIN_PATH
    '';

    dontWrapQtApps = true;
  };
  kwayland-integration = {
    nativeBuildInputs = [ pkgs.wayland-scanner ];
    buildInputs = with pkgs; [ wayland-protocols wayland ];
  };
  kwin = {
    nativeBuildInputs = with pkgs; [ python3 ];
    buildInputs = with pkgs;
      [
        hwdata
        lcms2
        libcap
        libdisplay-info
        libdrm
        libepoxy
        libinput
        libxkbcommon
        mesa
        pipewire
        udev
        wayland
        wayland-protocols
        xcb-util-cursor
        xorg.libICE
        xorg.libSM
        xorg.libxcvt
        xwayland
      ] ++ (with qt; [
        qtdeclarative
        qtmultimedia
        qtsensors
        qtvirtualkeyboard
        qt5compat
      ]) ++ (with kde; [ plasma-framework ])
      ++ (lib.optional (!isQt6) qt.qtx11extras);

    postPatch = if isQt6 then ''
      patchShebangs src/plugins/strip-effect-metadata.py
    '' else ''
      patchShebangs src/effects/strip-effect-metadata.py
    '';

    patches = [
      ./kwin/0001-follow-symlinks.patch
      ./kwin/0002-xwayland.patch
      ./kwin/0003-plugins-qpa-allow-using-nixos-wrapper.patch
      ./kwin/0001-NixOS-Unwrap-executable-name-for-.desktop-search.patch
      ./kwin/0001-Lower-CAP_SYS_NICE-from-the-ambient-set.patch
      # Pass special environments through arguemnts to `kwin_wayland`, bypassing
      # ld.so(8) environment stripping due to `kwin_wayland`'s capabilities.
      # We need this to have `TZDIR` correctly set for `plasmashell`, or
      # everything related to timezone, like clock widgets, will be broken.
      # https://invent.kde.org/plasma/kwin/-/merge_requests/1590
      (pkgs.fetchpatch {
        url =
          "https://invent.kde.org/plasma/kwin/-/commit/9a008b223ad696db3bf5692750f2b74e578e08b8.diff";
        sha256 = "sha256-f35G+g2MVABLDbAkCed3ZmtDWrzYn1rdD08mEx35j4k=";
      })
    ];

    CXXFLAGS = [ ''-DNIXPKGS_XWAYLAND=\"${lib.getExe pkgs.xwayland}\"'' ];
  };
  latte-dock = {
    buildInputs = [ pkgs.wayland pkgs.xorg.libSM ]
      ++ (lib.optional (!isQt6) qt.qtx11extras);
  };
  layer-shell-qt = { buildInputs = with pkgs; [ wayland wayland-protocols ]; };
  libkscreen = {
    nativeBuildInputs = [ pkgs.wayland-scanner ];
    buildInputs = with pkgs;
      [ wayland xorg.libXrandr ] ++ (lib.optional (!isQt6) qt.qtx11extras);
    # patches = [ ./libkscreen/libkscreen-backends-path.patch ];
    preConfigure = ''
      NIX_CFLAGS_COMPILE+=" -DNIXPKGS_LIBKSCREEN_BACKENDS=\"''${!outputBin}/$qtPluginPrefix/kf5/kscreen\""
    '';
    setupHook = propagate "out";
  };
  libksysguard = {
    patches = [ ./libksysguard/0001-qdiriterator-follow-symlinks.patch ];
    buildInputs = [
      pkgs.libnl
      pkgs.libpcap
      pkgs.lm_sensors
      qt.qt5compat
      qt.qtsensors
      qt.qtwebengine
    ] ++ (lib.optional (!isQt6) qt.qtx11extras);
    outputs = [ "bin" "dev" "out" ];
  };
  oxygen = {
    buildInputs = lib.optional (!isQt6) qt.qtx11extras;
    propagatedBuildInputs = [ qt.qtdeclarative pkgs.xorg.libXdmcp ];
    outputs = [ "bin" "dev" "out" ];
  };
  plank-player = { buildInputs = lib.optional (!isQt6) qt.qtquickcontrols2; };
  plasma-desktop = {
    buildInputs = with pkgs;
      [
        boost
        fontconfig
        ibus
        libcanberra_kde
        libpulseaudio
        qt.qt5compat
        wayland
        wayland-protocols
        xkeyboard_config
        xorg.libXcursor
        xorg.libXft
        xorg.libxkbfile
        xorg.xf86inputevdev
        xorg.xf86inputlibinput
        xorg.xf86inputsynaptics
        xorg.xinput
        xorg.xorgserver
      ] ++ (lib.optional (!isQt6) qt.qtx11extras);
    patches = [
      ./plasma-desktop/hwclock-path.patch
      ./plasma-desktop/tzdir.patch
      ./plasma-desktop/kcm-access.patch
    ];
    CXXFLAGS = let
      # run gsettings with desktop schemas for using in kcm_accces kcm
      gsettings-wrapper = pkgs.runCommandLocal "gsettings-wrapper" {
        nativeBuildInputs = [ pkgs.makeWrapper ];
      } ''
        makeWrapper ${pkgs.glib}/bin/gsettings $out --prefix XDG_DATA_DIRS : ${pkgs.gsettings-desktop-schemas.out}/share/gsettings-schemas/${pkgs.gsettings-desktop-schemas.name}
      '';
    in [
      ''-DNIXPKGS_HWCLOCK=\"${lib.getBin pkgs.util-linux}/bin/hwclock\"''
      ''-DNIXPKGS_GSETTINGS=\"${gsettings-wrapper}\"''
    ];
    postInstall = ''
      # Display ~/Desktop contents on the desktop by default.
      sed -i "''${!outputBin}/share/plasma/shells/org.kde.plasma.desktop/contents/defaults" \
          -e 's/Containment=org.kde.desktopcontainment/Containment=org.kde.plasma.folder/'
    '';
  };
  plasma-framework = {
    buildInputs = [ qt.qtsvg ] ++ (lib.optional (!isQt6) qt.qtquickcontrols2)
      ++ (lib.optional (!isQt6) qt.qtx11extras);
  };
  plasma-integration = {
    buildInputs = with pkgs;
      [ qt.qtsensors xorg.libXcursor wayland wayland-protocols ]
      ++ (lib.optional (!isQt6) qt.qtquickcontrols2)
      ++ (lib.optional (!isQt6) qt.qtx11extras);
    # workaround for a CI workaround
    preConfigure = lib.optional isQt6 "export CI_JOB_NAME_SLUG=qt6";
  };
  plasma-nano = { buildInputs = [ qt.qtsvg ]; };
  plasma-nm = { buildInputs = [ qt.qtsvg kde.qcoro ]; };
  plasma-pa = { buildInputs = [ pkgs.libcanberra_kde pkgs.libpulseaudio ]; };
  plasma-pass = { buildInputs = [ pkgs.oath-toolkit kde.gpgme ]; };
  plasma-welcome = {
    buildInputs = [ qt.qtsvg ] ++ (lib.optional (!isQt6) qt.qtquickcontrols2)
      ++ (lib.optional (!isQt6) pkgs.libsForQt5.accounts-qt)
      ++ (lib.optional (!isQt6) pkgs.libsForQt5.signond);
  };
  plasma-workspace = {
    passthru.providedSessions = [ "plasma" "plasmawayland" ];
    buildInputs = with pkgs;
      [
        isocodes
        kde.prison
        kde.kio-extras
        libcanberra_kde
        libdbusmenu
        libdrm
        libqalculate
        pam
        pipewire
        qt.qt5compat
        wayland
        wayland-protocols
        util-linux
        xorg.libSM
        xorg.libXcursor
        xorg.libXdmcp
        xorg.libXft
        xorg.libXtst
      ] ++ (lib.optional (!isQt6) qt.qtx11extras);
    propagatedUserEnvPkgs = [ qt.qt5compat ]
      ++ lib.optional (!isQt6) qt.qtgraphicaleffects;
    cmakeFlags = [
      "-DNIXPKGS_BREEZE_WALLPAPERS=${lib.getBin kde.breeze}/share/wallpapers"
    ];

    patches = [
      ./plasma-workspace/0001-startkde.patch
      ./plasma-workspace/0002-absolute-wallpaper-install-dir.patch
    ];

    # QT_INSTALL_BINS refers to qtbase, and qdbus is in qttools
    postPatch = ''
      substituteInPlace CMakeLists.txt \
        --replace 'ecm_query_qt(QtBinariesDir QT_INSTALL_BINS)' 'set(QtBinariesDir "${
          lib.getBin qt.qttools
        }/bin")'
    '';

    # work around wrapQtAppsHook double-wrapping kcminit_startup,
    # which is a symlink to kcminit
    postFixup = ''
      ln -sf $out/bin/kcminit $out/bin/kcminit_startup
    '';

    env.NIX_CFLAGS_COMPILE = toString [
      ''-DNIXPKGS_XMESSAGE="${lib.getBin pkgs.xorg.xmessage}/bin/xmessage"''
      ''-DNIXPKGS_XSETROOT="${lib.getBin pkgs.xorg.xsetroot}/bin/xsetroot"''
      #    ''
      #      -DNIXPKGS_START_KDEINIT_WRAPPER="${
      #        lib.getLib kinit
      #      }/libexec/kf5/start_kdeinit_wrapper"''
      #    ''-DNIXPKGS_KDEINIT5_SHUTDOWN="${lib.getBin kinit}/bin/kdeinit5_shutdown"''
    ];
  };
  plasma-workspace-wallpapers = { outputs = [ "out" ]; };
  powerdevil = { buildInputs = lib.optional (!isQt6) qt.qtx11extras; };
  qqc2-breeze-style = {
    buildInputs = lib.optional (!isQt6) qt.qtquickcontrols2;
  };
  xdg-desktop-portal-kde = {
    buildInputs = [ pkgs.wayland-protocols pkgs.wayland pkgs.cups ];
  };
}
