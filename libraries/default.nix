{ lib, pkgs, qt, kde, isQt6, ... }: {
  kirigami-addons = {
    buildInputs = lib.optional (!isQt6) qt.qtquickcontrols2;
  };
  kdsoap-ws-discovery-client = { buildInputs = [ kde.kdsoap ]; };
  kopeninghours = { buildInputs = [ pkgs.flex pkgs.bison ]; };
  kosmindoormap = { buildInputs = [ pkgs.flex pkgs.bison ]; };
  kpublictransport = { buildInputs = [ qt.qt5compat ]; };
  ktextaddons = { buildInputs = [ qt.qtspeech kde.qtkeychain ]; };
  kunifiedpush = { buildInputs = [ qt.qtwebsockets ]; };
  phonon = let
    debug = false;
    soname = "phonon4qt";
    buildsystemdir = "share/cmake/${soname}";

  in {
    buildInputs = with pkgs;
      [ libGLU libGL libpulseaudio ] ++ (with qt; [ qtbase qttools qt5compat ]);

    env.NIX_CFLAGS_COMPILE = "-fPIC";

    cmakeFlags = [
      "-DCMAKE_BUILD_TYPE=${if debug then "Debug" else "Release"}"
      (if isQt6 then "-DPHONON_BUILD_QT5=OFF" else "-DPHONON_BUILD_QT6=OFF")
    ];

    dontWrapQtApps = true;

    preConfigure = ''
      cmakeFlags+=" -DPHONON_QT_MKSPECS_INSTALL_DIR=''${!outputDev}/mkspecs"
      cmakeFlags+=" -DPHONON_QT_IMPORTS_INSTALL_DIR=''${!outputBin}/$qtQmlPrefix"
      cmakeFlags+=" -DPHONON_QT_PLUGIN_INSTALL_DIR=''${!outputBin}/$qtPluginPrefix/designer"
    '';

    postPatch = ''
      sed -i PhononConfig.cmake.in \
          -e "/get_filename_component(rootDir/ s/^.*$//" \
          -e "/^set(PHONON_INCLUDE_DIR/ s|\''${rootDir}/||" \
          -e "/^set(PHONON_LIBRARY_DIR/ s|\''${rootDir}/||" \
          -e "/^set(PHONON_BUILDSYSTEM_DIR/ s|\''${rootDir}|''${!outputDev}|"

      sed -i cmake/FindPhononInternal.cmake \
          -e "/set(INCLUDE_INSTALL_DIR/ c set(INCLUDE_INSTALL_DIR \"''${!outputDev}/include\")"

      sed -i cmake/FindPhononInternal.cmake \
          -e "/set(PLUGIN_INSTALL_DIR/ c set(PLUGIN_INSTALL_DIR \"$qtPluginPrefix/..\")"

      sed -i CMakeLists.txt \
          -e "/set(BUILDSYSTEM_INSTALL_DIR/ c set(BUILDSYSTEM_INSTALL_DIR \"''${!outputDev}/${buildsystemdir}\")"
    '';

    # postFixup = ''
    #  sed -i "''${!outputDev}/lib/pkgconfig/${soname}.pc" \
    #      -e "/^exec_prefix=/ c exec_prefix=''${!outputBin}/bin"
    # '';
  };
  polkit-qt-1 = {
    nativeBuildInputs = with pkgs; [ cmake pkg-config ];
    buildInputs = with pkgs;
      [ glib pcre polkit qt.qt5compat ]
      ++ lib.optionals stdenv.isLinux [ libselinux libsepol util-linux ];
    preConfigure = ''
      sed -i 's:''${exec_prefix}/@CMAKE_INSTALL_LIBDIR@:@CMAKE_INSTALL_FULL_LIBDIR@:' *.pc.cmake
      sed -i 's:''${prefix}/@CMAKE_INSTALL_INCLUDEDIR@:@CMAKE_INSTALL_FULL_INCLUDEDIR@:' *.pc.cmake
      sed -i 's:@PACKAGE_CMAKE_INSTALL_LIBDIR@:@CMAKE_INSTALL_FULL_LIBDIR@:' PolkitQt-1Config.cmake.in
      sed -i 's:@PACKAGE_LIB_DESTINATION@:@CMAKE_INSTALL_FULL_LIBDIR@:' PolkitQt-1Config.cmake.in
    '';
    cmakeFlags = lib.optional isQt6 "-DQT_MAJOR_VERSION=6";
  };
  qca = {
    buildInputs = [ pkgs.openssl qt.qt5compat ];
    nativeBuildInputs = with pkgs; [ cmake pkg-config ];
    outputs = [ "bin" "out" ];
    dontWrapQtApps = true;
    # tells CMake to use this CA bundle file if it is accessible
    preConfigure =
      "export QC_CERTSTORE_PATH=/etc/ssl/certs/ca-certificates.crt";
    # tricks CMake into using this CA bundle file if it is not accessible (in a sandbox)
    cmakeFlags = [ "-Dqca_CERTSTORE=/etc/ssl/certs/ca-certificates.crt" ];
  };
}
