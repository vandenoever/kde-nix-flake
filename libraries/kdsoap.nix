{ mkDerivation, pkgs, qtbase, isQt6 }:
mkDerivation rec {
  pname = "kdsoap";
  version = "2.1.1";

  src = pkgs.fetchurl {
    url =
      "https://github.com/KDAB/KDSoap/releases/download/kdsoap-${version}/kdsoap-${version}.tar.gz";
    sha256 = "sha256-rtV/ayAN33YvXSiY9+kijdBwCIHESRrv5ABvf6X1xic=";
  };

  outputs = [ "out" "dev" ];

  nativeBuildInputs = with pkgs; [ cmake pkg-config ];

  buildInputs = [ qtbase ];
  dontWrapQtApps = true;
  cmakeFlags = pkgs.lib.optional isQt6 [ "-DKDSoap_QT6=true" ];

  meta = with pkgs.lib; {
    description = "A Qt-based client-side and server-side SOAP component";
    longDescription = ''
      KD Soap is a Qt-based client-side and server-side SOAP component.

      It can be used to create client applications for web services and also
      provides the means to create web services without the need for any further
      component such as a dedicated web server.
    '';
    license = with licenses; [ gpl2 gpl3 lgpl21 ];
    maintainers = [ maintainers.ttuegel ];
  };
}
